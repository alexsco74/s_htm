<?php


namespace s;


class SHtm {

  public function getFromUrl($url) {
    $curl = curl_init();
    //https://www.sql.ru/forum/836041/curl-imitaciya-brauzera
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
    curl_setopt($curl, CURLOPT_NOBODY, 1);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    $htm = curl_exec($curl);
    return $htm;
  }

  public function getElementText($htm, $selector) {
    $tex = FALSE;

    if (substr($selector, 0, 1) !== '#') {
      return $tex;
    }
    $selector = trim($selector);

    $sel_tex = '"' . substr($selector, 1) . '"';
    $start = explode($sel_tex, $htm);
    if(count($start) !== 2){
      return $tex;
    }
//    include_once 'sites/all/libraries/s_go/src/sGo.php';
//    $s_go = new \Drupal\s_go\sGo();
//$s_go->createFile($start, 42);
    $end = explode('</', $start[1]);
    if(count($end) < 2){
      return $tex;
    }

    $inner = explode('>', $end[0]);
    if(count($inner) !== 2){
      return $tex;
    }
    $tex = trim($inner[1]);

    return $tex;
  }

}
